﻿using UnityAtoms;
using UnityEngine;

namespace MutCommon.Audio
{
  public class EnvelopeADSR : MonoBehaviour
  {
    private enum State
    {
      Attack,
      Decay,
      Sustain,
      Release,
    }

    private State state = State.Release;
    public BoolReference Gate;
    public FloatVariable Output;

    [Header("Parameters")]
    public FloatReference Attack;
    public FloatReference Decay;
    public FloatReference Sustain;
    private float sustain => Mathf.Clamp01(Sustain.Value);
    public FloatReference Release;

    void Update()
    {
      if (state == State.Attack)
      {
        var speed = 1.0f / Attack.Value;
        Output.Value += Time.deltaTime * speed;

        if (Output.Value > 1)
        {
          Output.Value = 1;
          state = State.Decay;
        }
        if (!Gate.Value)
        {
          state = State.Release;
        }
      }
      else if (state == State.Decay)
      {
        var speed = 1.0f / Decay.Value;
        Output.Value -= Time.deltaTime * speed;

        if (Output.Value < sustain)
        {
          Output.Value = sustain;
          state = State.Sustain;
        }
        if (!Gate.Value)
        {
          state = State.Release;
        }
      }
      else if (state == State.Sustain)
      {
        Output.Value = sustain;
        if (!Gate.Value)
        {
          state = State.Release;
        }
      }
      else if (state == State.Release)
      {
        var speed = 1.0f / Release.Value;
        Output.Value -= Time.deltaTime * speed;
        if (Gate.Value)
        {
          state = State.Attack;
        }
      }

      Output.Value = Mathf.Clamp01(Output.Value);
    }
  }
}