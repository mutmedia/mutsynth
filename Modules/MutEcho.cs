using System;
using FMOD;
using UnityEngine;
using UnityAtoms;

namespace MutCommon.Audio
{
  [RequireComponent(typeof(MutSynth))]
  public class MutEcho : MutInputDSP
  {
    [SerializeField]
    [Tooltip("[10, 5000]")]
    private FloatReference Delay;

    [SerializeField]
    [Tooltip("[0, 100]")]
    private FloatReference Feedback;

    [SerializeField]
    [Tooltip("[-80, 10]")]
    private FloatReference Drylevel;

    [SerializeField]
    [Tooltip("[-80, 10]")]
    private FloatReference Wetlevel;

    public override DSP_TYPE Type => DSP_TYPE.ECHO;

    public void Update()
    {
      dsp.setParameterFloat((int)DSP_ECHO.DELAY, Delay);
      dsp.setParameterFloat((int)DSP_ECHO.FEEDBACK, Feedback);
      dsp.setParameterFloat((int)DSP_ECHO.WETLEVEL, Wetlevel);
      dsp.setParameterFloat((int)DSP_ECHO.DRYLEVEL, Drylevel);
    }
  }
}
