using FMOD;
using UnityEngine;
using UnityAtoms;

namespace MutCommon.Audio
{
  public enum WaveType
  {
    sine = 0,
    square = 1,
    sawup = 2,
    sawdown = 3,
    triangle = 4,
    noise = 5,
  }

  [RequireComponent(typeof(MutSynth))]
  public class MutOscillator : MutInputDSP
  {
    [SerializeField]
    [Tooltip("[0:sine, 1:square, 2:sawup, 3:sawdown, 4:triangle, 5:noise] Shape of the sound wave")]
    public IntReference WaveType;

    [SerializeField]
    [Tooltip("[1, 22000] Frequency (note) of the sound wave")]
    public FloatReference Rate;

    public override DSP_TYPE Type => DSP_TYPE.OSCILLATOR;

    public void Update()
    {
      oscdsp.setParameterInt((int)DSP_OSCILLATOR.TYPE, WaveType);
      oscdsp.setParameterFloat((int)DSP_OSCILLATOR.RATE, Rate);
    }

    private DSP oscdsp;

    public override void Initialize(FMOD.System FMODSystem, ChannelGroup cg)
    {
      base.Initialize(FMODSystem, cg);
      oscdsp = this.dsp;


      FMODSystem.playDSP(this.oscdsp, cg, true, out var channel);
      this.DoNextFrame(() => channel.setPaused(false));
    }
  }
}
