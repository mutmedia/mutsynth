using FMOD;
using UnityEngine;

namespace MutCommon.Audio
{
  [RequireComponent(typeof(MutSynth))]
  public class MutMixer : MutInputDSP
  {
    public override DSP_TYPE Type => DSP_TYPE.MIXER;
  }
}
