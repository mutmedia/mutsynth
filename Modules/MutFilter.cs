using FMOD;
using UnityEngine;
using UnityAtoms;
using System.Collections.Generic;

namespace MutCommon.Audio
{
  public enum FilterType
  {
    LowPass,
    HighPass,
  }

  [RequireComponent(typeof(MutSynth))]
  public class MutFilter : MutInputDSP
  {
    [SerializeField]
    [Tooltip("[1, 22000] Cutoff frequency of the filter ")]
    public FloatReference Cutoff;

    [SerializeField]
    [Tooltip("[0, 10] Resonance of the filter ")]
    private FloatReference Resonance;

    [SerializeField]
    private FilterType Filter;

    static Dictionary<FilterType, FMOD.DSP_TYPE> typeMap = new Dictionary<FilterType, DSP_TYPE>() {
      { FilterType.LowPass, DSP_TYPE.LOWPASS },
      { FilterType.HighPass, DSP_TYPE.HIGHPASS }
    };

    static FMOD.DSP_TYPE Convert(FilterType type) => typeMap[type];

    public override DSP_TYPE Type => Convert(Filter);

    public void Update()
    {
      dsp.setParameterFloat((int)DSP_LOWPASS.CUTOFF, Cutoff);
      dsp.setParameterFloat((int)DSP_LOWPASS.RESONANCE, Resonance);
    }
  }
}

