﻿using UnityEngine;
using System;
using UnityAtoms;
using System.Linq;

namespace MutCommon.Audio
{
  public class MutSynth : MutDSP
  {
    // Start is called before the first frame update
    private FMOD.System FMODSystem;
    private FMOD.Sound sound;

    private FMOD.ChannelGroup master;

    public FloatReference MasterVolume;
    const FMOD.MODE soundMode = FMOD.MODE.LOOP_NORMAL | FMOD.MODE.CREATECOMPRESSEDSAMPLE | FMOD.MODE.NONBLOCKING;

    void Awake()
    {
      FMOD.Factory.System_Create(out FMODSystem);
      FMODSystem.setDSPBufferSize(1024, 10);
      FMODSystem.init(32, FMOD.INITFLAGS.NORMAL, (IntPtr)0);

      var soundResult = FMODSystem.createStream(Application.dataPath + "/Audio/voz.wav", FMOD.MODE.DEFAULT, out sound);

      FMODSystem.getMasterChannelGroup(out master);
      FMODSystem.createChannelGroup("mut", out var cg);
      // Let there be volume only on master channel
      cg.setVolume(0);
      master.setVolume(0);

      master.getDSP(FMOD.CHANNELCONTROL_DSP_INDEX.TAIL, out var dsp);
      this.dsp = dsp;
      this.dsp.setActive(true);

      var dsps = GetComponents<MutInputDSP>().ToList();

      // Initialize all childs
      dsps.ForEach(b => b.Initialize(FMODSystem, cg));

      // Then set their connections
      dsps.ForEach(b => b.SetConnection());

      /* 
          {
            FMODSystem.playSound(sound, cg, true, out var channel);
            channel.getDSP(FMOD.CHANNELCONTROL_DSP_INDEX.HEAD, out var dsp);
            channel.setLoopCount(-1);
            oscMixer.AddInput(dsp);
          }
      */
    }

    private void OnDestroy()
    {
      FMODSystem.getMasterChannelGroup(out var mcg);
      mcg.stop();
      master.release();
      FMODSystem.release();
    }

    float volume;
    float fadeSpeed = 1;
    // Update is called once per frame
    void Update()
    {
      volume = Mathf.Lerp(volume, MasterVolume.Value, fadeSpeed * Time.deltaTime);
      master.setVolume(volume);
    }
  }
}