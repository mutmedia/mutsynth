using FMOD;
using UnityEngine;

namespace MutCommon.Audio
{
  public abstract class MutDSP : MonoBehaviour
  {
    public DSP dsp;

    public virtual DSP_TYPE Type { get; }

    public virtual void Initialize(FMOD.System FMODSystem, ChannelGroup cg)
    {
      if (this.dsp.handle.ToInt32() == 0)
      {
        FMODSystem.createDSPByType(Type, out this.dsp);
      }

      this.dsp.setActive(true);
    }

    private static void ConnectTo(DSP dspA, DSP dspB)
    {
      var err = dspB.addInput(dspA, out var _, DSPCONNECTION_TYPE.STANDARD);
      UnityEngine.Debug.Log(err);
    }

    public MutDSP AddInput(MutDSP mutdsp)
    {
      ConnectTo(mutdsp.dsp, this.dsp);
      return mutdsp;
    }

    public void AddInput(DSP dsp)
      => ConnectTo(dsp, this.dsp);

    public void ConnectTo(MutDSP dsp)
      => ConnectTo(this.dsp, dsp.dsp);
  }
}