using FMOD;
using UnityEngine;
using UnityAtoms;

namespace MutCommon.Audio
{
  [RequireComponent(typeof(MutSynth))]
  public class MutChorus : MutInputDSP
  {
    [SerializeField]
    [Tooltip("[0, 100]")]
    private FloatReference Mix;

    [SerializeField]
    [Tooltip("[0, 20]")]
    private FloatReference Rate;

    [SerializeField]
    [Tooltip("[0, 100]")]
    private FloatReference Depth;

    public override DSP_TYPE Type => DSP_TYPE.CHORUS;

    public void Update()
    {
      dsp.setParameterFloat((int)DSP_CHORUS.MIX, Mix);
      dsp.setParameterFloat((int)DSP_CHORUS.RATE, Rate);
      dsp.setParameterFloat((int)DSP_CHORUS.DEPTH, Depth);
    }
  }
}
