using UnityEngine;

namespace MutCommon.Audio
{
  public abstract class MutInputDSP : MutDSP
  {
    [SerializeField]
    private MutDSP Output;

    public void SetConnection()
    {
      Output?.AddInput(this);
    }
  }
}